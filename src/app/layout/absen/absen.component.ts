import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-absen',
    templateUrl: './absen.component.html',
    styleUrls: ['./absen.component.scss']
})
export class AbsenComponent implements OnInit {

    private _data1: number;
    private _data2: number;
    private _dataPersen1: number;
    private _dataPersen2: number;
    private _dataTable: any;
    private _dataTableTotal1: any;
    private _dataTableTotal2: any;
    private _dataTableTotal: any;
    public _dataLoaded: boolean;
    public _dataLoaded_detail: boolean;

    public show: boolean;

    private _kode_wilayah: string;
    private _nama_wilayah: string;
    private _wilayah_latitude: string;
    private _wilayah_longitude: string;
    private _validasi: any;
    private _validasi_non: any;

    public donutChartData: any;
    private innerWidth: any;

    now: number;
    public provinsi: Observable<any[]>;
    public kota: Observable<any[]>;
    public kecamatan: Observable<any[]>;
    public kelurahan: Observable<any[]>;
    public tps: Observable<any[]>;
    public _provinsi: string;
    public _kota: string;
    public _kecamatan: string;
    public _kelurahan: string;
    public _tps: string;
    public _header_title: string;
    myForm: FormGroup;
    myForm2: FormGroup;
    closeResult: string;

    public pub_suara_1: number;
    public pub_suara_2: number;
    public pub_suara_scan_1: string;
    public pub_suara_scan_2: string;
    public pub_suara_scan_3: string;

    constructor(public db: AngularFirestore, private modalService: NgbModal) {
        this.myForm = new FormGroup({
            validasi: new FormControl(),
            provinsi: new FormControl(),
            kota: new FormControl(),
            kecamatan: new FormControl(),
            kelurahan: new FormControl(),
            tps: new FormControl(),
        });
        this.myForm2 = new FormGroup({
            hasil_suara_1: new FormControl(),
            hasil_suara_2: new FormControl()
        });

        this.myForm.get('validasi').setValue('false');
        this.myForm.get('provinsi').setValue('');
        this.myForm.get('kota').setValue('');
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');

        this.getProvinsi();
        this.getDataTPSDetail('test_user', '32.73', '');
        this.show = true;
        this.pub_suara_scan_1 = 'https://www.google.co.id/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';
        this.pub_suara_scan_2 = 'https://www.google.co.id/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';
        this.pub_suara_scan_3 = 'https://uploads.toptal.io/blog/image/125377/toptal-blog-image-1518184225493-b9ca8a8492da6fd38f11887c066c105e.png';
    }

    ngOnInit() {}

    open(content, data) {
        if (data) {

            this.pub_suara_1 = data.suara_1;
            this.pub_suara_2 = data.suara_2;
            this.pub_suara_scan_1 = data.suara_scan_1;
            this.pub_suara_scan_2 = data.suara_scan_2;
            this.pub_suara_scan_3 = data.suara_scan_3;
        }

        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    absen() {
        const form_value = this.myForm2.value;
        const form_json = JSON.stringify(form_value);
        console.log(form_json);
    }

    unabsen() {
        const form_value = this.myForm2.value;
        const form_json = JSON.stringify(form_value);
        console.log(form_json);
    }

    getProvinsi() {
        this.provinsi = this.db.collection('/test_provinsi', ref => ref.orderBy('nama_provinsi', 'asc')).valueChanges();
    }

    getKota(kode_provinsi) {
        this.kota = this.db.collection('/test_kota', ref => ref.where('kode_provinsi', '==', kode_provinsi).orderBy('nama_kota', 'asc')).valueChanges();
    }

    getKecamatan(kode_kota) {
        this.kecamatan = this.db.collection('/test_kecamatan', ref => ref.where('kode_kota', '==', kode_kota).orderBy('nama_kecamatan', 'asc')).valueChanges();
    }

    getKelurahan(kode_kecamatan) {
        this.kelurahan = this.db.collection('/test_kelurahan', ref => ref.where('kode_kecamatan', '==', kode_kecamatan).orderBy('nama_kelurahan', 'asc')).valueChanges();
    }

    getTPS(kode_kelurahan) {
        this.tps = this.db.collection('/test_tps', ref => ref.where('kode_kelurahan', '==', kode_kelurahan).orderBy('nama_tps', 'asc')).valueChanges();
    }

    onProvinsiChange(event) {
        this.kota = null;
        this.kecamatan = null;
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kota').setValue('');
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');

        const selectElementText = event.target['options']
            [event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._provinsi = selectElementText;
        } else {
            this._provinsi = null;
        }
        this._kota = null;
        this._kecamatan = null;
        this._kelurahan = null;
        this._tps = null;

        this.getKota(event.target.value);
    }

    onKotaChange(event) {
        this.kecamatan = null;
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');

        const selectElementText = event.target['options']
            [event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kota = selectElementText;
        } else {
            this._kota = null;
        }
        this._kecamatan = null;
        this._kelurahan = null;
        this._tps = null;

        this.getKecamatan(event.target.value);
    }

    onKecamatanChange(event) {
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');

        const selectElementText = event.target['options']
            [event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kecamatan = selectElementText;
        } else {
            this._kecamatan = null;
        }
        this._kelurahan = null;
        this._tps = null;

        this.getKelurahan(event.target.value);
    }

    onKelurahanChange(event) {
        this.tps = null;
        this.myForm.get('tps').setValue('');

        const selectElementText = event.target['options']
            [event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kelurahan = selectElementText;
        } else {
            this._kelurahan = null;
        }
        this._tps = null;

        this.getTPS(event.target.value);
    }

    // onTPSChange(event) {
    //     const selectElementText = event.target['options']
    //         [event.target['options'].selectedIndex].text;
    //     if (selectElementText !== '-- Pilih --') {
    //         this._tps = selectElementText;
    //     } else {
    //         this._tps = null;
    //     }
    // }

    onSubmit() {
        const form_value = this.myForm.value;
        const form_json = JSON.stringify(form_value);
        console.log(form_json);

        let validasi = 'validasi_non';
        if (form_value['validasi'] === 'true') {
            validasi = 'validasi';
        } else {
            validasi = 'validasi_non';
        }

        if (form_value['kelurahan'] !== '') {
            this.getDataTPSDetail('test_user', form_value['kelurahan'], validasi);
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan + ' - KEL ' + this._kelurahan;
          } else if (form_value['kecamatan'] !== '') {
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan;
          } else if (form_value['kota'] !== '') {
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota;
          } else if (form_value['provinsi'] !== '') {
            this._header_title = 'PROV ' + this._provinsi;
          } else {
            this._header_title = 'Nasional';
          }
    }

    getDataTPSDetail(level, kode, validasi) {

        const table_json = [];

        //  get Data
        this.db.collection('/' + level)
        .valueChanges()
        .subscribe(item => {
          console.log(item);

          item.forEach(it => {
            const temp = {};

            temp['kode_wilayah'] = it['kode_tps'];
            temp['nama_wilayah'] = it['nama_tps'];
            temp['status_absen'] = it['status_absen'];
            temp['status_kirim'] = it['status_kirim'];
            temp['latitude'] = it['latitude'];
            temp['longitude'] = it['longitude'];
            table_json.push(temp);
          });

          this._dataLoaded_detail = true;
          this._dataTable = table_json;

        });

    }
}
