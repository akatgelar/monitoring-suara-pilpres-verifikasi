import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AbsenRoutingModule } from './absen-routing.module';
import { AbsenComponent } from './absen.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, AbsenRoutingModule, FormsModule, ReactiveFormsModule],
    declarations: [AbsenComponent]
})
export class AbsenModule {}
