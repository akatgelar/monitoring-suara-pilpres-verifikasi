import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AbsenComponent } from './absen.component';

const routes: Routes = [
    {
        path: '',
        component: AbsenComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AbsenRoutingModule {}
