import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'peta', pathMatch: 'prefix' },
            { path: 'peta', loadChildren: './peta/peta.module#PetaModule' },
            { path: 'verifikasi', loadChildren: './verifikasi/verifikasi.module#VerifikasiModule' },
            { path: 'absen', loadChildren: './absen/absen.module#AbsenModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
