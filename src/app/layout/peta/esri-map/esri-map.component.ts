import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { loadModules } from 'esri-loader';
import esri = __esri; // Esri TypeScript Types

@Component({
  selector: 'app-esri-map',
  templateUrl: './esri-map.component.html',
  styleUrls: ['./esri-map.component.css']
})
export class EsriMapComponent implements OnInit {

  @Output() mapLoadedEvent = new EventEmitter<boolean>();

  // The <div> where we will place the map
  @ViewChild('mapViewNode') private mapViewEl: ElementRef;
  @ViewChild('infoDiv') private infoDiv: ElementRef;

  private paramZoom = 13;
  private paramCenter: Array<number> = [-6.882691, 107.610764];
  private paramBasemap = 'gray';
  private paramIsloaded = false;
  private paramGround = 'world-elevation';

  private EsriMap: any;
  private EsriMapView: any;
  private EsriSceneView: any;
  private EsriLocator: any;
  private EsriExpand: any;
  private EsriBasemapToggle: any;
  private EsriBasemapGallery: any;
  private EsriFeatureLayer: any;
  private EsriNavigationToggle: any;
  private EsriPoint: any;
  private EsriPolygon: any;
  private EsriLegend: any;
  private EsriConfig: any;
  private EsriRequest: any;
  private EsriGraphicsLayer: any;
  private EsriGraphic: any;
  private EsriLabelClass: any;

  private maps: any;
  private mapView: any;
  private locatorTask: any;
  private layer: any;
  private legend: any;


  get mapLoaded(): boolean {
    return this.paramIsloaded;
  }

  @Input()
  set zoom(zoom: number) {
    this.paramZoom = zoom;
  }

  get zoom(): number {
    return this.paramZoom;
  }

  @Input()
  set center(center: Array<number>) {
    this.paramCenter = center;
  }

  get center(): Array<number> {
    return this.paramCenter;
  }

  @Input()
  set basemap(basemap: string) {
    this.paramBasemap = basemap;
  }

  get basemap(): string {
    return this.paramBasemap;
  }

  @Input()
  set ground(ground: string) {
    this.paramGround = ground;
  }

  get ground(): string {
    return this.paramGround;
  }


  constructor() {
  }

  async initializeMap() {
    try {

      // 3D
      const [EsriMap, EsriMapView, EsriSceneView, EsriLocator, EsriExpand, EsriBasemapToggle,
        EsriBasemapGallery, EsriFeatureLayer, EsriNavigationToggle,
        EsriPoint, EsriPolygon, EsriLegend, EsriConfig, EsriRequest,
        EsriGraphicsLayer, EsriGraphic, EsriLabelClass] = await loadModules([
        'esri/Map',
        'esri/views/MapView',
        'esri/views/SceneView',
        'esri/tasks/Locator',
        'esri/widgets/Expand',
        'esri/widgets/BasemapToggle',
        'esri/widgets/BasemapGallery',
        'esri/layers/FeatureLayer',
        'esri/widgets/NavigationToggle',
        'esri/geometry/Point',
        'esri/geometry/Polygon',
        'esri/widgets/Legend',
        'esri/config',
        'esri/request',
        'esri/layers/GraphicsLayer',
        'esri/Graphic',
        'esri/layers/support/LabelClass',
      ]);

      this.EsriMap = EsriMap;
      this.EsriMapView = EsriMapView;
      this.EsriSceneView = EsriSceneView;
      this.EsriLocator = EsriLocator;
      this.EsriExpand = EsriExpand;
      this.EsriBasemapToggle = EsriBasemapToggle;
      this.EsriBasemapGallery = EsriBasemapGallery;
      this.EsriFeatureLayer = EsriFeatureLayer;
      this.EsriNavigationToggle = EsriNavigationToggle;
      this.EsriPoint = EsriPoint;
      this.EsriPolygon = EsriPolygon;
      this.EsriLegend = EsriLegend;
      this.EsriConfig = EsriConfig;
      this.EsriRequest = EsriRequest;
      this.EsriGraphicsLayer = EsriGraphicsLayer;
      this.EsriGraphic = EsriGraphic;
      this.EsriLabelClass = EsriLabelClass;

      // Configure the Map
      const mapProperties: esri.MapProperties = {
        basemap: this.paramBasemap,
        ground: this.paramGround
      };

      this.maps = new this.EsriMap(mapProperties);

      // Initialize the MapView
      const mapViewProperties: esri.MapViewProperties = {
        container: this.mapViewEl.nativeElement,
        center: this.paramCenter,
        zoom: this.paramZoom,
        map: this.maps,
        constraints: {
          snapToZoom: true
        },
        ui: {
          padding: {
            bottom: 15,
            right: 0
          }
        }
      };

      // 2D
      // this.mapView = new this.EsriMapView(mapViewProperties);

      // 3D
      this.mapView = new this.EsriSceneView(mapViewProperties);

      // locatortask
      this.locatorTask = new this.EsriLocator({
        url: 'https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer'
      });

      const basemapGallery = new this.EsriBasemapGallery({
        view: this.mapView,
        source: {
          portal: {
            url: 'http://www.arcgis.com',
            useVectorBasemaps: false  // Load vector tile basemaps
          }
        }
      });

      const bgExpand = new this.EsriExpand({
        expandIconClass: 'esri-icon-collection',
        expandTooltip: 'Basemaps',
        view: this.mapView,
        content: basemapGallery
      });

      // close the expand whenever a basemap is selected
      basemapGallery.watch('activeBasemap', () => {
        const mobileSize = this.mapView.heightBreakpoint === 'xsmall' || this.mapView.widthBreakpoint === 'xsmall';
        if (mobileSize) {
          bgExpand.collapse();
        }
      });

      this.mapView.ui.add(bgExpand, 'top-right');

      this.mapView.ui.move([ 'zoom', 'navigation-toggle', 'compass' ], 'top-right');

      return this.mapView;

    } catch (error) {
      console.log('EsriLoader: ', error);
    }

  }



  // Finalize a few things once the MapView has been loaded
  houseKeeping(mapView) {
    this.mapView.when(() => {
      console.log('mapView ready: ', mapView.ready);
      this.paramIsloaded = mapView.ready;
      this.mapLoadedEvent.emit(true);

      // this.setPopUp();
      this.basePolygon();
      this.getData();
      this.getDataSaksi();

    });


  }


  setPopUp() {

    this.mapView.popup.autoOpenEnabled = false;
    this.mapView.on('click', (event: any) => {
      // Get the coordinates of the click on the view
      // around the decimals to 3 decimals
      const lat = Math.round(event.mapPoint.latitude * 1000) / 1000;
      const lon = Math.round(event.mapPoint.longitude * 1000) / 1000;

      // Display the popup
      this.mapView.popup.open({
        title: 'Reverse geocode: [' + lon + ',' + lat + ']',
        location: event.mapPoint,
        content: ''
      });

      this.locatorTask.locationToAddress(event.mapPoint).then((response) => {
        this.mapView.popup.content = response.address;
      }).catch((error) => {
        this.mapView.popup.content =
          'No address was found for this location';
      });
    });
  }

  basePolygon() {
    console.log('basePolygon');

    const graphicsLayer = new this.EsriGraphicsLayer();
    const url = 'assets/map/area-kelurahan.json';
    this.EsriRequest(url, {responseType: 'json'}).then((geoJson) => {

      geoJson.data.features.map((feature, i) => {

        let varRings = [];
        if (feature.geometry.coordinates.length === 1) {
          varRings = feature.geometry.coordinates[0];
        } else {
          for (let j = 0; j < feature.geometry.coordinates.length; j++) {
            varRings[j] = feature.geometry.coordinates[j][0];
          }
        }

        const polygon = new this.EsriPolygon({
          hasM: true,
          rings: varRings,
          spatialReference: {wkid : 4326}
        });
        // console.log(polygon);

        const fillSymbol = {
          type: 'simple-fill', // autocasts as new SimpleFillSymbol()
          color: [134, 131, 180, 0.8],
          outline: { // autocasts as new SimpleLineSymbol()
            color: [255, 255, 255],
            width: 1
          }
        };

        const attribute = {
          ObjectID: i,
          id: feature.properties.id,
          kode_kelurahan: feature.properties.kode_kelurahan,
          nama_kelurahan: feature.properties.nama_kelurahan,
          nama_kecamatan: feature.properties.nama_kecamatan,
          nama_kota: feature.properties.nama_kota,
          nama_provinsi: feature.properties.nama_provinsi
        };

        const popupTemplates = {
          type: 'PopupTemplate',
          title: 'Kabupaten / Kota',
          content: [{
            type: 'fields',
            fieldInfos: [{
              fieldName: 'id',
              label: 'id',
              visible: true
            }, {
              fieldName: 'kode_kelurahan',
              label: 'Kode Kelurahan',
              visible: true
            }, {
              fieldName: 'nama_kelurahan',
              label: 'Nama Kelurahan',
              visible: true
            }, {
              fieldName: 'nama_kecamatan',
              label: 'Nama Kecamatan',
              visible: true
            }, {
              fieldName: 'nama_kota',
              label: 'Nama Kota',
              visible: true
            }, {
              fieldName: 'nama_provinsi',
              label: 'Nama Provinsi',
              visible: true
            }]
          }]
        };

        const polygonGraphic = {
          type: 'Graphic',
          geometry: polygon,
          symbol: fillSymbol,
          attributes: attribute,
          popupTemplate: popupTemplates
        };

        graphicsLayer.add(polygonGraphic);
      });

      this.maps.add(graphicsLayer);
    });
  }

  getData() {
    console.log('getData');
    console.log('no param');

    const url = 'assets/map/titik-tps.json';
    // this.EsriConfig.request.corsEnabledServers.push(url);
    this.EsriRequest(url, {responseType: 'json'}).then((response) => {
      this.createGraphics(response);
    });
  }

  createGraphics(response) {
    console.log('createGraphics');
    console.log(response.data);

    // raw GeoJSON data
    const geoJson = response.data;
    const varfeatures = [];
    // Create an array of Graphics from each GeoJSON feature
    geoJson.features.map((feature, i) => {
      let graphics: any;
      graphics = {
        type: 'Graphic',
        geometry: {
          type: 'point', // autocasts as new Point()
          x: feature.geometry.coordinates[0],
          y: feature.geometry.coordinates[1]
        },
        attributes: {
          ObjectID: i,
          id: feature.properties.id,
          kode_tps: feature.properties.kode_tps,
          nama_provinsi: feature.properties.nama_provinsi,
          nama_kota: feature.properties.nama_kota,
          nama_kecamatan: feature.properties.nama_kecamatan,
          nama_kelurahan: feature.properties.nama_kelurahan,
          nama_tps: feature.properties.nama_tps,
          suara_1: feature.properties.suara_1,
          suara_2: feature.properties.suara_2,
          suara_scan_1: feature.properties.suara_scan_1,
          suara_scan_2: feature.properties.suara_scan_2,
          suara_scan_3: feature.properties.suara_scan_3
        },
      };

      varfeatures.push(graphics);
    });

    this.createLayer(varfeatures);
  }

  createLayer(graphics) {
    console.log('createLayer');
    console.log(graphics);

    const varFields = [
      {
        name: 'ObjectID',
        alias: 'ObjectID',
        type: 'oid'
      }, {
        name: 'id',
        alias: 'id',
        type: 'integer'
      }, {
        name: 'kode_tps',
        alias: 'kode_tps',
        type: 'string'
      }, {
        name: 'nama_provinsi',
        alias: 'nama_provinsi',
        type: 'string'
      }, {
        name: 'nama_kota',
        alias: 'nama_kota',
        type: 'string'
      }, {
        name: 'nama_kecamatan',
        alias: 'nama_kecamatan',
        type: 'string'
      }, {
        name: 'nama_kelurahan',
        alias: 'nama_kelurahan',
        type: 'string'
      }, {
        name: 'nama_tps',
        alias: 'nama_tps',
        type: 'string'
      }, {
        name: 'suara_1',
        alias: 'suara_1',
        type: 'integer'
      }, {
        name: 'suara_2',
        alias: 'suara_2',
        type: 'integer'
      }, {
        name: 'suara_scan_1',
        alias: 'suara_scan_1',
        type: 'string'
      }, {
        name: 'suara_scan_2',
        alias: 'suara_scan_2',
        type: 'string'
      }, {
        name: 'suara_scan_3',
        alias: 'suara_scan_3',
        type: 'string'
      }
    ];

    const varTemplate = {
      title: '{nama_tps}',
      content: [{
        type: 'fields',
        fieldInfos: [{
          fieldName: 'id',
          label: 'id',
          visible: true
        }, {
          fieldName: 'kode_tps',
          label: 'kode_tps',
          visible: true
        }, {
          fieldName: 'nama_provinsi',
          label: 'nama_provinsi',
          visible: true
        }, {
          fieldName: 'nama_kota',
          label: 'nama_kota',
          visible: true
        }, {
          fieldName: 'nama_kecamatan',
          label: 'nama_kecamatan',
          visible: true
        }, {
          fieldName: 'nama_kelurahan',
          label: 'nama_kelurahan',
          visible: true
        }, {
          fieldName: 'nama_tps',
          label: 'nama_tps',
          visible: true
        }, {
          fieldName: 'suara_1',
          label: 'suara_1',
          visible: true
        }, {
          fieldName: 'suara_2',
          label: 'suara_2',
          visible: true
        }, {
          fieldName: 'suara_scan_1',
          label: 'suara_scan_1',
          visible: true
        }, {
          fieldName: 'suara_scan_2',
          label: 'suara_scan_2',
          visible: true
        }, {
          fieldName: 'suara_scan_3',
          label: 'suara_scan_3',
          visible: true
        }]
      }]
    };

    const varQuakesRenderers = {
      type: 'simple', // autocasts as new SimpleRenderer()
      symbol: {
        type: 'simple-marker', // autocasts as new SimpleMarkerSymbol()
        style: 'circle',
        size: 10,
        color: [255, 0, 85, 0.5],
        outline: {
          width: 1,
          color: '#FF0055',
          style: 'solid'
        }
      },
    };

    const labelClass = {
      // autocasts as new LabelClass()
      symbol: {
        type: 'text',  // autocasts as new TextSymbol()
        color: '#FF0055',
        haloSize: '1px',
        text: '1',
        xoffset: 3,
        yoffset: 3,
        font: {  // autocast as new Font()
          size: 8,
          family: 'sans-serif',
        }
      },
      labelPlacement: 'above-center',
      labelExpressionInfo: {
        expression: '$feature.nama_tps'
      }
    };

    this.layer = new this.EsriFeatureLayer({
      source: graphics, // autocast as an array of esri/Graphic
      fields: varFields, // This is required when creating a layer from Graphics
      objectIdField: 'ObjectID', // This must be defined when creating a layer from Graphics
      renderer: varQuakesRenderers, // set the visualization on the layer
      popupTemplate: varTemplate,
      labelingInfo: [ labelClass ]

    });
    this.createLegend(this.layer);
  }

  createLegend(layers) {
    console.log('createLegend');
    console.log(layers);
    this.legend = new this.EsriLegend({
      view: this.mapView,
      layerInfos: [
      {
        layer: layers,
        title: 'TPS'
      }]
    });

    this.legend.style = {
      type: 'classic',
      layout: 'auto'
    };

    const bgExpand = new this.EsriExpand({
      expandIconClass: 'esri-icon-description',
      expandTooltip: 'Info Legend',
      view: this.mapView,
      content: this.legend
    });

    this.maps.add(layers);
    this.mapView.ui.add(bgExpand, 'bottom-right');
    this.mapView.ui.padding = {top: 20, left: 20, right: 20, bottom: 40};

  }

  getDataSaksi() {
    console.log('getData');
    console.log('no param');

    const url = '/api/trackingSaksi';
    // this.EsriConfig.request.corsEnabledServers.push(url);
    this.EsriRequest(url, {responseType: 'json'}).then((response) => {
      this.createGraphicsSaksi(response);
    });
  }

  createGraphicsSaksi(response) {
    console.log('createGraphics');
    console.log(response.data);

    // raw GeoJSON data
    const geoJson = response.data;
    const varfeatures = [];
    // Create an array of Graphics from each GeoJSON feature
    geoJson.features.map((feature, i) => {
      let graphics: any;
      graphics = {
        type: 'Graphic',
        geometry: {
          type: 'point', // autocasts as new Point()
          x: feature.geometry.coordinates[0],
          y: feature.geometry.coordinates[1]
        },
        attributes: {
          ObjectID: i,
          id: feature.properties.id,
          kode_tps: feature.properties.kode_tps,
          nama_provinsi: feature.properties.nama_provinsi,
          nama_kota: feature.properties.nama_kota,
          nama_kecamatan: feature.properties.nama_kecamatan,
          nama_kelurahan: feature.properties.nama_kelurahan,
          nama_tps: feature.properties.nama_tps,
          nama: feature.properties.nama,
          no_hp: feature.properties.no_hp,
          last_access: feature.properties.last_access,
        },
      };

      varfeatures.push(graphics);
    });

    this.createLayerSaksi(varfeatures);
  }

  createLayerSaksi(graphics) {
    console.log('createLayer');
    console.log(graphics);

    const varFields = [
      {
        name: 'ObjectID',
        alias: 'ObjectID',
        type: 'oid'
      }, {
        name: 'id',
        alias: 'id',
        type: 'integer'
      }, {
        name: 'kode_tps',
        alias: 'kode_tps',
        type: 'string'
      }, {
        name: 'nama_provinsi',
        alias: 'nama_provinsi',
        type: 'string'
      }, {
        name: 'nama_kota',
        alias: 'nama_kota',
        type: 'string'
      }, {
        name: 'nama_kecamatan',
        alias: 'nama_kecamatan',
        type: 'string'
      }, {
        name: 'nama_kelurahan',
        alias: 'nama_kelurahan',
        type: 'string'
      }, {
        name: 'nama_tps',
        alias: 'nama_tps',
        type: 'string'
      }, {
        name: 'nama',
        alias: 'nama',
        type: 'string'
      }, {
        name: 'no_hp',
        alias: 'no_hp',
        type: 'string'
      }, {
        name: 'last_access',
        alias: 'last_access',
        type: 'string'
      }
    ];

    const varTemplate = {
      title: '{nama}',
      content: [{
        type: 'fields',
        fieldInfos: [{
          fieldName: 'id',
          label: 'id',
          visible: true
        }, {
          fieldName: 'kode_tps',
          label: 'kode_tps',
          visible: true
        }, {
          fieldName: 'nama_provinsi',
          label: 'nama_provinsi',
          visible: true
        }, {
          fieldName: 'nama_kota',
          label: 'nama_kota',
          visible: true
        }, {
          fieldName: 'nama_kecamatan',
          label: 'nama_kecamatan',
          visible: true
        }, {
          fieldName: 'nama_kelurahan',
          label: 'nama_kelurahan',
          visible: true
        }, {
          fieldName: 'nama_tps',
          label: 'nama_tps',
          visible: true
        }, {
          fieldName: 'nama',
          label: 'nama',
          visible: true
        }, {
          fieldName: 'no_hp',
          label: 'no_hp',
          visible: true
        }, {
          fieldName: 'last_access',
          label: 'last_access',
          visible: true
        }]
      }]
    };

    const varQuakesRenderers = {
      type: 'simple', // autocasts as new SimpleRenderer()
      symbol: {
        type: 'simple-marker', // autocasts as new SimpleMarkerSymbol()
        style: 'circle',
        size: 5,
        color: [0, 0, 0, 0.5],
        outline: {
          width: 1,
          color: '#000000',
          style: 'solid'
        }
      },
    };

    const labelClass = {
      // autocasts as new LabelClass()
      symbol: {
        type: 'text',  // autocasts as new TextSymbol()
        color: '#000000',
        haloSize: '1px',
        text: '1',
        xoffset: 3,
        yoffset: 3,
        font: {  // autocast as new Font()
          size: 8,
          family: 'sans-serif',
        }
      },
      labelPlacement: 'above-center',
      labelExpressionInfo: {
        expression: '$feature.nama'
      }
    };

    this.layer = new this.EsriFeatureLayer({
      source: graphics, // autocast as an array of esri/Graphic
      fields: varFields, // This is required when creating a layer from Graphics
      objectIdField: 'ObjectID', // This must be defined when creating a layer from Graphics
      renderer: varQuakesRenderers, // set the visualization on the layer
      popupTemplate: varTemplate,
      labelingInfo: [ labelClass ]

    });
    this.createLegendSaksi(this.layer);
  }

  createLegendSaksi(layers) {
    console.log('createLegend');
    console.log(layers);
    this.legend = new this.EsriLegend({
      view: this.mapView,
      layerInfos: [
      {
        layer: layers,
        title: 'Saksi'
      }]
    });

    this.legend.style = {
      type: 'classic',
      layout: 'auto'
    };

    const bgExpand = new this.EsriExpand({
      expandIconClass: 'esri-icon-description',
      expandTooltip: 'Info Legend',
      view: this.mapView,
      content: this.legend
    });

    this.maps.add(layers);
    this.mapView.ui.add(bgExpand, 'bottom-right');
    this.mapView.ui.padding = {top: 20, left: 20, right: 20, bottom: 40};

  }

  errback(error) {
    console.error('Creating legend failed. ', error);
  }

  ngOnInit() {
    // Initialize MapView and return an instance of MapView
    this.initializeMap().then((result) => {
      this.houseKeeping(result);
    });
  }

}
