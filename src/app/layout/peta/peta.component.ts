import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-peta',
    templateUrl: './peta.component.html',
    styleUrls: ['./peta.component.scss'],
    animations: [routerTransition()]
})
export class PetaComponent implements OnInit {

    public innerHeight: any;
    // Set our map properties
    mapCenter = [107.574, -6.819];
    basemapType = 'osm';
    mapZoomLevel = 13;

    // See app.component.html
    mapLoadedEvent(status: boolean) {
      console.log('The map loaded: ' + status);
    }

    ngOnInit() {
        this.innerHeight = window.innerHeight;
    }

}
