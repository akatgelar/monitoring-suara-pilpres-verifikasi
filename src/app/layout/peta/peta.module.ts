import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PetaRoutingModule } from './peta-routing.module';
import { PetaComponent } from './peta.component';
import { EsriMapComponent } from './esri-map/esri-map.component';

@NgModule({
    imports: [CommonModule, PetaRoutingModule],
    declarations: [PetaComponent, EsriMapComponent]
})
export class PetaModule {}
