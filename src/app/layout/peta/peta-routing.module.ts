import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PetaComponent } from './peta.component';

const routes: Routes = [
    {
        path: '',
        component: PetaComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PetaRoutingModule {}
