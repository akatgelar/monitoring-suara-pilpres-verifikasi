import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';

@Component({
    selector: 'app-verifikasi',
    templateUrl: './verifikasi.component.html',
    styleUrls: ['./verifikasi.component.scss']
})
export class VerifikasiComponent implements OnInit {

    private _data1: number;
    private _data2: number;
    private _dataPersen1: number;
    private _dataPersen2: number;
    private _dataTable: any;
    private _dataTableTotal1: any;
    private _dataTableTotal2: any;
    private _dataTableTotal: any;
    public _dataLoaded: boolean;
    public _dataLoaded_detail: boolean;

    public show: boolean;

    private _kode_wilayah: string;
    private _nama_wilayah: string;
    private _wilayah_latitude: string;
    private _wilayah_longitude: string;
    private _validasi: any;
    private _validasi_non: any;

    public donutChartData: any;
    private innerWidth: any;

    now: number;
    public provinsi: Observable<any[]>;
    public kota: Observable<any[]>;
    public kecamatan: Observable<any[]>;
    public kelurahan: Observable<any[]>;
    public tps: Observable<any[]>;
    public _provinsi: string;
    public _kota: string;
    public _kecamatan: string;
    public _kelurahan: string;
    public _tps: string;
    public _header_title: string;
    myForm: FormGroup;
    myForm2: FormGroup;
    closeResult: string;

    public pub_suara_1: number;
    public pub_suara_2: number;
    public pub_suara_sah: number;
    public pub_suara_tidaksah: number;
    public pub_suara_total: number;
    public pub_suara_scan_1: string;
    public pub_suara_scan_2: string;
    public pub_suara_scan_3: string;

    constructor(public db: AngularFirestore, private modalService: NgbModal,
        private http: HttpClient) {
        this.myForm = new FormGroup({
            validasi: new FormControl(),
            provinsi: new FormControl(),
            kota: new FormControl(),
            kecamatan: new FormControl(),
            kelurahan: new FormControl(),
            tps: new FormControl(),
        });
        this.myForm2 = new FormGroup({
            suara_1: new FormControl(),
            suara_2: new FormControl(),
            suara_sah: new FormControl(),
            suara_tidaksah: new FormControl(),
            suara_total: new FormControl(),
            kode_tps: new FormControl(),
        });

        this.myForm.get('validasi').setValue('false');
        this.myForm.get('provinsi').setValue('');
        this.myForm.get('kota').setValue('');
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');

        this.getProvinsi();
        this.getDataTPSDetail('test_tps', '32.73', '');
        this.show = true;
        this.pub_suara_scan_1 = 'https://www.google.co.id/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';
        this.pub_suara_scan_2 = 'https://www.google.co.id/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';
        this.pub_suara_scan_3 = 'https://uploads.toptal.io/blog/image/125377/toptal-blog-image-1518184225493-b9ca8a8492da6fd38f11887c066c105e.png';
    }

    ngOnInit() {}

    open(content, data) {
        if (data) {

            console.log(data);

            this.pub_suara_1 = data.suara_1;
            this.pub_suara_2 = data.suara_2;
            this.pub_suara_sah = data.suara_sah;
            this.pub_suara_tidaksah = data.suara_tidaksah;
            this.pub_suara_total = data.suara_total;
            this.pub_suara_scan_1 = data.suara_scan_1;
            this.pub_suara_scan_2 = data.suara_scan_2;
            this.pub_suara_scan_3 = data.suara_scan_3;

            this.myForm2.patchValue({
                suara_1: data.suara_1,
                suara_2: data.suara_2,
                suara_sah: data.suara_sah,
                suara_tidaksah: data.suara_tidaksah,
                suara_total: data.suara_total,
                kode_tps: data.kode_wilayah,
            });
        }

        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    verifikasi() {
        const form_value = this.myForm2.value;
        const form_json = JSON.stringify(form_value);
        console.log(form_json);
    }

    unverifikasi() {
        const form_value = this.myForm2.value;
        const form_json = JSON.stringify(form_value);
        console.log(form_json);
    }

    getProvinsi() {
        this.provinsi = this.db.collection('/test_provinsi', ref => ref.orderBy('nama_provinsi', 'asc')).valueChanges();
    }

    getKota(kode_provinsi) {
        this.kota = this.db.collection('/test_kota', ref => ref.where('kode_provinsi', '==', kode_provinsi).orderBy('nama_kota', 'asc')).valueChanges();
    }

    getKecamatan(kode_kota) {
        this.kecamatan = this.db.collection('/test_kecamatan', ref => ref.where('kode_kota', '==', kode_kota).orderBy('nama_kecamatan', 'asc')).valueChanges();
    }

    getKelurahan(kode_kecamatan) {
        this.kelurahan = this.db.collection('/test_kelurahan', ref => ref.where('kode_kecamatan', '==', kode_kecamatan).orderBy('nama_kelurahan', 'asc')).valueChanges();
    }

    getTPS(kode_kelurahan) {
        this.tps = this.db.collection('/test_tps', ref => ref.where('kode_kelurahan', '==', kode_kelurahan).orderBy('nama_tps', 'asc')).valueChanges();
    }

    onProvinsiChange(event) {
        this.kota = null;
        this.kecamatan = null;
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kota').setValue('');
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');

        const selectElementText = event.target['options']
            [event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._provinsi = selectElementText;
        } else {
            this._provinsi = null;
        }
        this._kota = null;
        this._kecamatan = null;
        this._kelurahan = null;
        this._tps = null;

        this.getKota(event.target.value);
    }

    onKotaChange(event) {
        this.kecamatan = null;
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');

        const selectElementText = event.target['options']
            [event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kota = selectElementText;
        } else {
            this._kota = null;
        }
        this._kecamatan = null;
        this._kelurahan = null;
        this._tps = null;

        this.getKecamatan(event.target.value);
    }

    onKecamatanChange(event) {
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');

        const selectElementText = event.target['options']
            [event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kecamatan = selectElementText;
        } else {
            this._kecamatan = null;
        }
        this._kelurahan = null;
        this._tps = null;

        this.getKelurahan(event.target.value);
    }

    onKelurahanChange(event) {
        this.tps = null;
        this.myForm.get('tps').setValue('');

        const selectElementText = event.target['options']
            [event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kelurahan = selectElementText;
        } else {
            this._kelurahan = null;
        }
        this._tps = null;

        this.getTPS(event.target.value);
    }

    // onTPSChange(event) {
    //     const selectElementText = event.target['options']
    //         [event.target['options'].selectedIndex].text;
    //     if (selectElementText !== '-- Pilih --') {
    //         this._tps = selectElementText;
    //     } else {
    //         this._tps = null;
    //     }
    // }

    onSubmit() {
        const form_value = this.myForm.value;
        const form_json = JSON.stringify(form_value);
        console.log(form_json);

        let validasi = 'validasi_non';
        if (form_value['validasi'] === 'true') {
            validasi = 'validasi';
        } else {
            validasi = 'validasi_non';
        }

        if (form_value['kelurahan'] !== '') {
            this.getDataTPSDetail('test_tps', form_value['kelurahan'], validasi);
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan + ' - KEL ' + this._kelurahan;
          } else if (form_value['kecamatan'] !== '') {
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan;
          } else if (form_value['kota'] !== '') {
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota;
          } else if (form_value['provinsi'] !== '') {
            this._header_title = 'PROV ' + this._provinsi;
          } else {
            this._header_title = 'Nasional';
          }
    }

    onSubmit2() {
        const form_value2 = this.myForm2.value;
        const form_json2 = JSON.stringify(form_value2);
        console.log(form_json2);

        const url = '/api/verifikasiSuaraTPS';
        const data = form_json2;

        this.sendData(url, data).subscribe(
            async result => {
                console.log(result);
                // if error
            },
            async error => {
                console.log('error');
                console.log(error);
            }
        );
    }

    sendData(url: string, data: any): Observable<any> {
        console.log(url);
        console.log(data);
        const header = { 'Content-Type': 'application/json'};
        return this.http.post(url, data, { headers : new HttpHeaders(header) })
        .pipe(
            map(result => {
                // console.log('httpPost result');
                // console.log(result);
                location.reload();
                return result;
            })
        );

    }

    getDataTPSDetail(level, kode, validasi) {

        const table_json = [];
        let total1 = 0;
        let total2 = 0;
        let total = 0;

        //  get Data
        this.db.collection('/' + level)
        .valueChanges()
        .subscribe(item => {
          console.log(item);

          item.forEach(it => {
            const temp = {};

            temp['kode_wilayah'] = it['kode_tps'];
            temp['nama_wilayah'] = it['nama_tps'];
            temp['suara_1'] = it['suara_1'];
            temp['suara_2'] = it['suara_2'];
            temp['suara_sah'] = it['suara_sah'];
            temp['suara_tidaksah'] = it['suara_tidaksah'];
            temp['suara_scan_1'] = it['suara_scan_1'];
            temp['suara_scan_2'] = it['suara_scan_2'];
            temp['suara_scan_3'] = it['suara_scan_3'];
            temp['status_validasi'] = it['status_validasi'];
            temp['suara_total'] = Number(it['suara_1']) + Number(it['suara_2']);
            total1 = total1 + Number(temp['suara_1']);
            total2 = total2 + Number(temp['suara_2']);
            total = Number(total1) + Number(total2);
            table_json.push(temp);
          });

          this._dataLoaded_detail = true;
          this._dataTable = table_json;
          this._dataTableTotal1 = total1;
          this._dataTableTotal2 = total2;
          this._dataTableTotal = total;

        });

    }
}
