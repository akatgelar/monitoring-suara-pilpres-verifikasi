import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerifikasiRoutingModule } from './verifikasi-routing.module';
import { VerifikasiComponent } from './verifikasi.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, VerifikasiRoutingModule, FormsModule, ReactiveFormsModule],
    declarations: [VerifikasiComponent]
})
export class VerifikasiModule {}
