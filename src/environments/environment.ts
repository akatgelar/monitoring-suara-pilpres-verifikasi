// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.


export const environment = {
  production: false,
  firebase: {
      apiKey: 'AIzaSyByj3yvcMqUWb_8r1icUni28y4KRxr3M-I',
      authDomain: 'monitoring-suara-pilpres-2019.firebaseapp.com',
      databaseURL: 'https://monitoring-suara-pilpres-2019.firebaseio.com',
      projectId: 'monitoring-suara-pilpres-2019',
      storageBucket: 'monitoring-suara-pilpres-2019.appspot.com',
      messagingSenderId: '919294575946'
  }
};
