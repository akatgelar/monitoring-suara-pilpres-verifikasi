(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["peta-peta-module"],{

/***/ "./node_modules/esri-loader/dist/umd/esri-loader.js":
/*!**********************************************************!*\
  !*** ./node_modules/esri-loader/dist/umd/esri-loader.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
	 true ? factory(exports) :
	undefined;
}(this, (function (exports) { 'use strict';

function createStylesheetLink(url) {
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = url;
    return link;
}
// TODO: export this function?
// check if the css url has been injected or added manually
function getCss(url) {
    return document.querySelector("link[href*=\"" + url + "\"]");
}
// lazy load the CSS needed for the ArcGIS API
function loadCss(url) {
    var link = getCss(url);
    if (!link) {
        // create & load the css library
        link = createStylesheetLink(url);
        document.head.appendChild(link);
    }
    return link;
}

/*
  Copyright 2017 Esri
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
var isBrowser = typeof window !== 'undefined';
var DEFAULT_URL = 'https://js.arcgis.com/4.10/';
// this is the url that is currently being, or already has loaded
var _currentUrl;
function createScript(url) {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;
    script.setAttribute('data-esri-loader', 'loading');
    return script;
}
// add a one-time load handler to script
// and optionally add a one time error handler as well
function handleScriptLoad(script, callback, errback) {
    var onScriptError;
    if (errback) {
        // set up an error handler as well
        onScriptError = handleScriptError(script, errback);
    }
    var onScriptLoad = function () {
        // pass the script to the callback
        callback(script);
        // remove this event listener
        script.removeEventListener('load', onScriptLoad, false);
        if (onScriptError) {
            // remove the error listener as well
            script.removeEventListener('error', onScriptError, false);
        }
    };
    script.addEventListener('load', onScriptLoad, false);
}
// add a one-time error handler to the script
function handleScriptError(script, callback) {
    var onScriptError = function (e) {
        // reject the promise and remove this event listener
        callback(e.error || new Error("There was an error attempting to load " + script.src));
        // remove this event listener
        script.removeEventListener('error', onScriptError, false);
    };
    script.addEventListener('error', onScriptError, false);
    return onScriptError;
}
// allow consuming libraries to provide their own Promise implementations
var utils = {
    Promise: isBrowser ? window['Promise'] : undefined
};
// get the script injected by this library
function getScript() {
    return document.querySelector('script[data-esri-loader]');
}
// has ArcGIS API been loaded on the page yet?
function isLoaded() {
    var globalRequire = window['require'];
    // .on() ensures that it's Dojo's AMD loader
    return globalRequire && globalRequire.on;
}
// load the ArcGIS API on the page
function loadScript(options) {
    if (options === void 0) { options = {}; }
    // default options
    if (!options.url) {
        options.url = DEFAULT_URL;
    }
    return new utils.Promise(function (resolve, reject) {
        var script = getScript();
        if (script) {
            // the API is already loaded or in the process of loading...
            // NOTE: have to test against scr attribute value, not script.src
            // b/c the latter will return the full url for relative paths
            var src = script.getAttribute('src');
            if (src !== options.url) {
                // potentially trying to load a different version of the API
                reject(new Error("The ArcGIS API for JavaScript is already loaded (" + src + ")."));
            }
            else {
                if (isLoaded()) {
                    // the script has already successfully loaded
                    resolve(script);
                }
                else {
                    // wait for the script to load and then resolve
                    handleScriptLoad(script, resolve, reject);
                }
            }
        }
        else {
            if (isLoaded()) {
                // the API has been loaded by some other means
                // potentially trying to load a different version of the API
                reject(new Error("The ArcGIS API for JavaScript is already loaded."));
            }
            else {
                // this is the first time attempting to load the API
                if (options.css) {
                    // load the css before loading the script
                    loadCss(options.css);
                }
                if (options.dojoConfig) {
                    // set dojo configuration parameters before loading the script
                    window['dojoConfig'] = options.dojoConfig;
                }
                // create a script object whose source points to the API
                script = createScript(options.url);
                _currentUrl = options.url;
                // once the script is loaded...
                handleScriptLoad(script, function () {
                    // update the status of the script
                    script.setAttribute('data-esri-loader', 'loaded');
                    // return the script
                    resolve(script);
                }, reject);
                // load the script
                document.body.appendChild(script);
            }
        }
    });
}
// wrap dojo's require() in a promise
function requireModules(modules) {
    return new utils.Promise(function (resolve, reject) {
        // If something goes wrong loading the esri/dojo scripts, reject with the error.
        var errorHandler = window['require'].on('error', reject);
        window['require'](modules, function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            // remove error handler
            errorHandler.remove();
            // Resolve with the parameters from dojo require as an array.
            resolve(args);
        });
    });
}
// returns a promise that resolves with an array of the required modules
// also will attempt to lazy load the ArcGIS API if it has not already been loaded
function loadModules(modules, loadScriptOptions) {
    if (loadScriptOptions === void 0) { loadScriptOptions = {}; }
    if (!isLoaded()) {
        // script is not yet loaded
        if (!loadScriptOptions.url && _currentUrl) {
            // alredy in the process of loading, so default to the same url
            loadScriptOptions.url = _currentUrl;
        }
        // attept to load the script then load the modules
        return loadScript(loadScriptOptions).then(function () { return requireModules(modules); });
    }
    else {
        // script is already loaded, just load the modules
        return requireModules(modules);
    }
}
// NOTE: rollup ignores the default export
// and builds the UMD namespace out of named exports
// so this is only needed so that consumers of the ESM build
// can do esriLoader.loadModules(), etc
// TODO: remove this next breaking change?
var esriLoader = {
    getScript: getScript,
    isLoaded: isLoaded,
    loadModules: loadModules,
    loadScript: loadScript,
    loadCss: loadCss,
    // TODO: export getCss too?
    utils: utils
};

exports.utils = utils;
exports.getScript = getScript;
exports.isLoaded = isLoaded;
exports.loadScript = loadScript;
exports.loadModules = loadModules;
exports['default'] = esriLoader;
exports.loadCss = loadCss;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=esri-loader.js.map


/***/ }),

/***/ "./src/app/layout/peta/esri-map/esri-map.component.css":
/*!*************************************************************!*\
  !*** ./src/app/layout/peta/esri-map/esri-map.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n@import url('https://js.arcgis.com/4.10/esri/css/main.css');\n/* import the required JSAPI css */\n.esri-view {\n  height: 100%;\n}\n.viewDiv {\n  padding: 0;\n  margin: 0;\n  height: 100%;\n  width: 100%;\n}\n.infoDiv {\n  position: absolute;\n  bottom: 15px;\n  right: 0;\n  max-height: 80%;\n  max-width: 300px;\n  background-color: black;\n  padding: 8px;\n  border-top-left-radius: 5px;\n  color: white;\n  opacity: 0.8;\n}"

/***/ }),

/***/ "./src/app/layout/peta/esri-map/esri-map.component.html":
/*!**************************************************************!*\
  !*** ./src/app/layout/peta/esri-map/esri-map.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Here's the DIV that we inject the map into -->\n<div #mapViewNode class=\"viewDiv\"></div>\n<!-- <div #infoDiv class=\"infoDiv\"></div> -->"

/***/ }),

/***/ "./src/app/layout/peta/esri-map/esri-map.component.ts":
/*!************************************************************!*\
  !*** ./src/app/layout/peta/esri-map/esri-map.component.ts ***!
  \************************************************************/
/*! exports provided: EsriMapComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EsriMapComponent", function() { return EsriMapComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var esri_loader__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! esri-loader */ "./node_modules/esri-loader/dist/umd/esri-loader.js");
/* harmony import */ var esri_loader__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(esri_loader__WEBPACK_IMPORTED_MODULE_1__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


var EsriMapComponent = /** @class */ (function () {
    function EsriMapComponent() {
        this.mapLoadedEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.paramZoom = 13;
        this.paramCenter = [-6.882691, 107.610764];
        this.paramBasemap = 'gray';
        this.paramIsloaded = false;
        this.paramGround = 'world-elevation';
    }
    Object.defineProperty(EsriMapComponent.prototype, "mapLoaded", {
        get: function () {
            return this.paramIsloaded;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EsriMapComponent.prototype, "zoom", {
        get: function () {
            return this.paramZoom;
        },
        set: function (zoom) {
            this.paramZoom = zoom;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EsriMapComponent.prototype, "center", {
        get: function () {
            return this.paramCenter;
        },
        set: function (center) {
            this.paramCenter = center;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EsriMapComponent.prototype, "basemap", {
        get: function () {
            return this.paramBasemap;
        },
        set: function (basemap) {
            this.paramBasemap = basemap;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EsriMapComponent.prototype, "ground", {
        get: function () {
            return this.paramGround;
        },
        set: function (ground) {
            this.paramGround = ground;
        },
        enumerable: true,
        configurable: true
    });
    EsriMapComponent.prototype.initializeMap = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var _a, EsriMap, EsriMapView, EsriSceneView, EsriLocator, EsriExpand, EsriBasemapToggle, EsriBasemapGallery, EsriFeatureLayer, EsriNavigationToggle, EsriPoint, EsriPolygon, EsriLegend, EsriConfig, EsriRequest, EsriGraphicsLayer, EsriGraphic, EsriLabelClass, mapProperties, mapViewProperties, basemapGallery, bgExpand_1, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Object(esri_loader__WEBPACK_IMPORTED_MODULE_1__["loadModules"])([
                                'esri/Map',
                                'esri/views/MapView',
                                'esri/views/SceneView',
                                'esri/tasks/Locator',
                                'esri/widgets/Expand',
                                'esri/widgets/BasemapToggle',
                                'esri/widgets/BasemapGallery',
                                'esri/layers/FeatureLayer',
                                'esri/widgets/NavigationToggle',
                                'esri/geometry/Point',
                                'esri/geometry/Polygon',
                                'esri/widgets/Legend',
                                'esri/config',
                                'esri/request',
                                'esri/layers/GraphicsLayer',
                                'esri/Graphic',
                                'esri/layers/support/LabelClass',
                            ])];
                    case 1:
                        _a = _b.sent(), EsriMap = _a[0], EsriMapView = _a[1], EsriSceneView = _a[2], EsriLocator = _a[3], EsriExpand = _a[4], EsriBasemapToggle = _a[5], EsriBasemapGallery = _a[6], EsriFeatureLayer = _a[7], EsriNavigationToggle = _a[8], EsriPoint = _a[9], EsriPolygon = _a[10], EsriLegend = _a[11], EsriConfig = _a[12], EsriRequest = _a[13], EsriGraphicsLayer = _a[14], EsriGraphic = _a[15], EsriLabelClass = _a[16];
                        this.EsriMap = EsriMap;
                        this.EsriMapView = EsriMapView;
                        this.EsriSceneView = EsriSceneView;
                        this.EsriLocator = EsriLocator;
                        this.EsriExpand = EsriExpand;
                        this.EsriBasemapToggle = EsriBasemapToggle;
                        this.EsriBasemapGallery = EsriBasemapGallery;
                        this.EsriFeatureLayer = EsriFeatureLayer;
                        this.EsriNavigationToggle = EsriNavigationToggle;
                        this.EsriPoint = EsriPoint;
                        this.EsriPolygon = EsriPolygon;
                        this.EsriLegend = EsriLegend;
                        this.EsriConfig = EsriConfig;
                        this.EsriRequest = EsriRequest;
                        this.EsriGraphicsLayer = EsriGraphicsLayer;
                        this.EsriGraphic = EsriGraphic;
                        this.EsriLabelClass = EsriLabelClass;
                        mapProperties = {
                            basemap: this.paramBasemap,
                            ground: this.paramGround
                        };
                        this.maps = new this.EsriMap(mapProperties);
                        mapViewProperties = {
                            container: this.mapViewEl.nativeElement,
                            center: this.paramCenter,
                            zoom: this.paramZoom,
                            map: this.maps,
                            constraints: {
                                snapToZoom: true
                            },
                            ui: {
                                padding: {
                                    bottom: 15,
                                    right: 0
                                }
                            }
                        };
                        // 2D
                        // this.mapView = new this.EsriMapView(mapViewProperties);
                        // 3D
                        this.mapView = new this.EsriSceneView(mapViewProperties);
                        // locatortask
                        this.locatorTask = new this.EsriLocator({
                            url: 'https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer'
                        });
                        basemapGallery = new this.EsriBasemapGallery({
                            view: this.mapView,
                            source: {
                                portal: {
                                    url: 'http://www.arcgis.com',
                                    useVectorBasemaps: false // Load vector tile basemaps
                                }
                            }
                        });
                        bgExpand_1 = new this.EsriExpand({
                            expandIconClass: 'esri-icon-collection',
                            expandTooltip: 'Basemaps',
                            view: this.mapView,
                            content: basemapGallery
                        });
                        // close the expand whenever a basemap is selected
                        basemapGallery.watch('activeBasemap', function () {
                            var mobileSize = _this.mapView.heightBreakpoint === 'xsmall' || _this.mapView.widthBreakpoint === 'xsmall';
                            if (mobileSize) {
                                bgExpand_1.collapse();
                            }
                        });
                        this.mapView.ui.add(bgExpand_1, 'top-right');
                        this.mapView.ui.move(['zoom', 'navigation-toggle', 'compass'], 'top-right');
                        return [2 /*return*/, this.mapView];
                    case 2:
                        error_1 = _b.sent();
                        console.log('EsriLoader: ', error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // Finalize a few things once the MapView has been loaded
    EsriMapComponent.prototype.houseKeeping = function (mapView) {
        var _this = this;
        this.mapView.when(function () {
            console.log('mapView ready: ', mapView.ready);
            _this.paramIsloaded = mapView.ready;
            _this.mapLoadedEvent.emit(true);
            // this.setPopUp();
            _this.basePolygon();
            _this.getData();
            _this.getDataSaksi();
        });
    };
    EsriMapComponent.prototype.setPopUp = function () {
        var _this = this;
        this.mapView.popup.autoOpenEnabled = false;
        this.mapView.on('click', function (event) {
            // Get the coordinates of the click on the view
            // around the decimals to 3 decimals
            var lat = Math.round(event.mapPoint.latitude * 1000) / 1000;
            var lon = Math.round(event.mapPoint.longitude * 1000) / 1000;
            // Display the popup
            _this.mapView.popup.open({
                title: 'Reverse geocode: [' + lon + ',' + lat + ']',
                location: event.mapPoint,
                content: ''
            });
            _this.locatorTask.locationToAddress(event.mapPoint).then(function (response) {
                _this.mapView.popup.content = response.address;
            }).catch(function (error) {
                _this.mapView.popup.content =
                    'No address was found for this location';
            });
        });
    };
    EsriMapComponent.prototype.basePolygon = function () {
        var _this = this;
        console.log('basePolygon');
        var graphicsLayer = new this.EsriGraphicsLayer();
        var url = 'assets/map/area-kelurahan.json';
        this.EsriRequest(url, { responseType: 'json' }).then(function (geoJson) {
            geoJson.data.features.map(function (feature, i) {
                var varRings = [];
                if (feature.geometry.coordinates.length === 1) {
                    varRings = feature.geometry.coordinates[0];
                }
                else {
                    for (var j = 0; j < feature.geometry.coordinates.length; j++) {
                        varRings[j] = feature.geometry.coordinates[j][0];
                    }
                }
                var polygon = new _this.EsriPolygon({
                    hasM: true,
                    rings: varRings,
                    spatialReference: { wkid: 4326 }
                });
                // console.log(polygon);
                var fillSymbol = {
                    type: 'simple-fill',
                    color: [134, 131, 180, 0.8],
                    outline: {
                        color: [255, 255, 255],
                        width: 1
                    }
                };
                var attribute = {
                    ObjectID: i,
                    id: feature.properties.id,
                    kode_kelurahan: feature.properties.kode_kelurahan,
                    nama_kelurahan: feature.properties.nama_kelurahan,
                    nama_kecamatan: feature.properties.nama_kecamatan,
                    nama_kota: feature.properties.nama_kota,
                    nama_provinsi: feature.properties.nama_provinsi
                };
                var popupTemplates = {
                    type: 'PopupTemplate',
                    title: 'Kabupaten / Kota',
                    content: [{
                            type: 'fields',
                            fieldInfos: [{
                                    fieldName: 'id',
                                    label: 'id',
                                    visible: true
                                }, {
                                    fieldName: 'kode_kelurahan',
                                    label: 'Kode Kelurahan',
                                    visible: true
                                }, {
                                    fieldName: 'nama_kelurahan',
                                    label: 'Nama Kelurahan',
                                    visible: true
                                }, {
                                    fieldName: 'nama_kecamatan',
                                    label: 'Nama Kecamatan',
                                    visible: true
                                }, {
                                    fieldName: 'nama_kota',
                                    label: 'Nama Kota',
                                    visible: true
                                }, {
                                    fieldName: 'nama_provinsi',
                                    label: 'Nama Provinsi',
                                    visible: true
                                }]
                        }]
                };
                var polygonGraphic = {
                    type: 'Graphic',
                    geometry: polygon,
                    symbol: fillSymbol,
                    attributes: attribute,
                    popupTemplate: popupTemplates
                };
                graphicsLayer.add(polygonGraphic);
            });
            _this.maps.add(graphicsLayer);
        });
    };
    EsriMapComponent.prototype.getData = function () {
        var _this = this;
        console.log('getData');
        console.log('no param');
        var url = 'assets/map/titik-tps.json';
        // this.EsriConfig.request.corsEnabledServers.push(url);
        this.EsriRequest(url, { responseType: 'json' }).then(function (response) {
            _this.createGraphics(response);
        });
    };
    EsriMapComponent.prototype.createGraphics = function (response) {
        console.log('createGraphics');
        console.log(response.data);
        // raw GeoJSON data
        var geoJson = response.data;
        var varfeatures = [];
        // Create an array of Graphics from each GeoJSON feature
        geoJson.features.map(function (feature, i) {
            var graphics;
            graphics = {
                type: 'Graphic',
                geometry: {
                    type: 'point',
                    x: feature.geometry.coordinates[0],
                    y: feature.geometry.coordinates[1]
                },
                attributes: {
                    ObjectID: i,
                    id: feature.properties.id,
                    kode_tps: feature.properties.kode_tps,
                    nama_provinsi: feature.properties.nama_provinsi,
                    nama_kota: feature.properties.nama_kota,
                    nama_kecamatan: feature.properties.nama_kecamatan,
                    nama_kelurahan: feature.properties.nama_kelurahan,
                    nama_tps: feature.properties.nama_tps,
                    suara_1: feature.properties.suara_1,
                    suara_2: feature.properties.suara_2,
                    suara_scan_1: feature.properties.suara_scan_1,
                    suara_scan_2: feature.properties.suara_scan_2,
                    suara_scan_3: feature.properties.suara_scan_3
                },
            };
            varfeatures.push(graphics);
        });
        this.createLayer(varfeatures);
    };
    EsriMapComponent.prototype.createLayer = function (graphics) {
        console.log('createLayer');
        console.log(graphics);
        var varFields = [
            {
                name: 'ObjectID',
                alias: 'ObjectID',
                type: 'oid'
            }, {
                name: 'id',
                alias: 'id',
                type: 'integer'
            }, {
                name: 'kode_tps',
                alias: 'kode_tps',
                type: 'string'
            }, {
                name: 'nama_provinsi',
                alias: 'nama_provinsi',
                type: 'string'
            }, {
                name: 'nama_kota',
                alias: 'nama_kota',
                type: 'string'
            }, {
                name: 'nama_kecamatan',
                alias: 'nama_kecamatan',
                type: 'string'
            }, {
                name: 'nama_kelurahan',
                alias: 'nama_kelurahan',
                type: 'string'
            }, {
                name: 'nama_tps',
                alias: 'nama_tps',
                type: 'string'
            }, {
                name: 'suara_1',
                alias: 'suara_1',
                type: 'integer'
            }, {
                name: 'suara_2',
                alias: 'suara_2',
                type: 'integer'
            }, {
                name: 'suara_scan_1',
                alias: 'suara_scan_1',
                type: 'string'
            }, {
                name: 'suara_scan_2',
                alias: 'suara_scan_2',
                type: 'string'
            }, {
                name: 'suara_scan_3',
                alias: 'suara_scan_3',
                type: 'string'
            }
        ];
        var varTemplate = {
            title: '{nama_tps}',
            content: [{
                    type: 'fields',
                    fieldInfos: [{
                            fieldName: 'id',
                            label: 'id',
                            visible: true
                        }, {
                            fieldName: 'kode_tps',
                            label: 'kode_tps',
                            visible: true
                        }, {
                            fieldName: 'nama_provinsi',
                            label: 'nama_provinsi',
                            visible: true
                        }, {
                            fieldName: 'nama_kota',
                            label: 'nama_kota',
                            visible: true
                        }, {
                            fieldName: 'nama_kecamatan',
                            label: 'nama_kecamatan',
                            visible: true
                        }, {
                            fieldName: 'nama_kelurahan',
                            label: 'nama_kelurahan',
                            visible: true
                        }, {
                            fieldName: 'nama_tps',
                            label: 'nama_tps',
                            visible: true
                        }, {
                            fieldName: 'suara_1',
                            label: 'suara_1',
                            visible: true
                        }, {
                            fieldName: 'suara_2',
                            label: 'suara_2',
                            visible: true
                        }, {
                            fieldName: 'suara_scan_1',
                            label: 'suara_scan_1',
                            visible: true
                        }, {
                            fieldName: 'suara_scan_2',
                            label: 'suara_scan_2',
                            visible: true
                        }, {
                            fieldName: 'suara_scan_3',
                            label: 'suara_scan_3',
                            visible: true
                        }]
                }]
        };
        var varQuakesRenderers = {
            type: 'simple',
            symbol: {
                type: 'simple-marker',
                style: 'circle',
                size: 10,
                color: [255, 0, 85, 0.5],
                outline: {
                    width: 1,
                    color: '#FF0055',
                    style: 'solid'
                }
            },
        };
        var labelClass = {
            // autocasts as new LabelClass()
            symbol: {
                type: 'text',
                color: '#FF0055',
                haloSize: '1px',
                text: '1',
                xoffset: 3,
                yoffset: 3,
                font: {
                    size: 8,
                    family: 'sans-serif',
                }
            },
            labelPlacement: 'above-center',
            labelExpressionInfo: {
                expression: '$feature.nama_tps'
            }
        };
        this.layer = new this.EsriFeatureLayer({
            source: graphics,
            fields: varFields,
            objectIdField: 'ObjectID',
            renderer: varQuakesRenderers,
            popupTemplate: varTemplate,
            labelingInfo: [labelClass]
        });
        this.createLegend(this.layer);
    };
    EsriMapComponent.prototype.createLegend = function (layers) {
        console.log('createLegend');
        console.log(layers);
        this.legend = new this.EsriLegend({
            view: this.mapView,
            layerInfos: [
                {
                    layer: layers,
                    title: 'TPS'
                }
            ]
        });
        this.legend.style = {
            type: 'classic',
            layout: 'auto'
        };
        var bgExpand = new this.EsriExpand({
            expandIconClass: 'esri-icon-description',
            expandTooltip: 'Info Legend',
            view: this.mapView,
            content: this.legend
        });
        this.maps.add(layers);
        this.mapView.ui.add(bgExpand, 'bottom-right');
        this.mapView.ui.padding = { top: 20, left: 20, right: 20, bottom: 40 };
    };
    EsriMapComponent.prototype.getDataSaksi = function () {
        var _this = this;
        console.log('getData');
        console.log('no param');
        var url = '/api/trackingSaksi';
        // this.EsriConfig.request.corsEnabledServers.push(url);
        this.EsriRequest(url, { responseType: 'json' }).then(function (response) {
            _this.createGraphicsSaksi(response);
        });
    };
    EsriMapComponent.prototype.createGraphicsSaksi = function (response) {
        console.log('createGraphics');
        console.log(response.data);
        // raw GeoJSON data
        var geoJson = response.data;
        var varfeatures = [];
        // Create an array of Graphics from each GeoJSON feature
        geoJson.features.map(function (feature, i) {
            var graphics;
            graphics = {
                type: 'Graphic',
                geometry: {
                    type: 'point',
                    x: feature.geometry.coordinates[0],
                    y: feature.geometry.coordinates[1]
                },
                attributes: {
                    ObjectID: i,
                    id: feature.properties.id,
                    kode_tps: feature.properties.kode_tps,
                    nama_provinsi: feature.properties.nama_provinsi,
                    nama_kota: feature.properties.nama_kota,
                    nama_kecamatan: feature.properties.nama_kecamatan,
                    nama_kelurahan: feature.properties.nama_kelurahan,
                    nama_tps: feature.properties.nama_tps,
                    nama: feature.properties.nama,
                    no_hp: feature.properties.no_hp,
                    last_access: feature.properties.last_access,
                },
            };
            varfeatures.push(graphics);
        });
        this.createLayerSaksi(varfeatures);
    };
    EsriMapComponent.prototype.createLayerSaksi = function (graphics) {
        console.log('createLayer');
        console.log(graphics);
        var varFields = [
            {
                name: 'ObjectID',
                alias: 'ObjectID',
                type: 'oid'
            }, {
                name: 'id',
                alias: 'id',
                type: 'integer'
            }, {
                name: 'kode_tps',
                alias: 'kode_tps',
                type: 'string'
            }, {
                name: 'nama_provinsi',
                alias: 'nama_provinsi',
                type: 'string'
            }, {
                name: 'nama_kota',
                alias: 'nama_kota',
                type: 'string'
            }, {
                name: 'nama_kecamatan',
                alias: 'nama_kecamatan',
                type: 'string'
            }, {
                name: 'nama_kelurahan',
                alias: 'nama_kelurahan',
                type: 'string'
            }, {
                name: 'nama_tps',
                alias: 'nama_tps',
                type: 'string'
            }, {
                name: 'nama',
                alias: 'nama',
                type: 'string'
            }, {
                name: 'no_hp',
                alias: 'no_hp',
                type: 'string'
            }, {
                name: 'last_access',
                alias: 'last_access',
                type: 'string'
            }
        ];
        var varTemplate = {
            title: '{nama}',
            content: [{
                    type: 'fields',
                    fieldInfos: [{
                            fieldName: 'id',
                            label: 'id',
                            visible: true
                        }, {
                            fieldName: 'kode_tps',
                            label: 'kode_tps',
                            visible: true
                        }, {
                            fieldName: 'nama_provinsi',
                            label: 'nama_provinsi',
                            visible: true
                        }, {
                            fieldName: 'nama_kota',
                            label: 'nama_kota',
                            visible: true
                        }, {
                            fieldName: 'nama_kecamatan',
                            label: 'nama_kecamatan',
                            visible: true
                        }, {
                            fieldName: 'nama_kelurahan',
                            label: 'nama_kelurahan',
                            visible: true
                        }, {
                            fieldName: 'nama_tps',
                            label: 'nama_tps',
                            visible: true
                        }, {
                            fieldName: 'nama',
                            label: 'nama',
                            visible: true
                        }, {
                            fieldName: 'no_hp',
                            label: 'no_hp',
                            visible: true
                        }, {
                            fieldName: 'last_access',
                            label: 'last_access',
                            visible: true
                        }]
                }]
        };
        var varQuakesRenderers = {
            type: 'simple',
            symbol: {
                type: 'simple-marker',
                style: 'circle',
                size: 5,
                color: [0, 0, 0, 0.5],
                outline: {
                    width: 1,
                    color: '#000000',
                    style: 'solid'
                }
            },
        };
        var labelClass = {
            // autocasts as new LabelClass()
            symbol: {
                type: 'text',
                color: '#000000',
                haloSize: '1px',
                text: '1',
                xoffset: 3,
                yoffset: 3,
                font: {
                    size: 8,
                    family: 'sans-serif',
                }
            },
            labelPlacement: 'above-center',
            labelExpressionInfo: {
                expression: '$feature.nama'
            }
        };
        this.layer = new this.EsriFeatureLayer({
            source: graphics,
            fields: varFields,
            objectIdField: 'ObjectID',
            renderer: varQuakesRenderers,
            popupTemplate: varTemplate,
            labelingInfo: [labelClass]
        });
        this.createLegendSaksi(this.layer);
    };
    EsriMapComponent.prototype.createLegendSaksi = function (layers) {
        console.log('createLegend');
        console.log(layers);
        this.legend = new this.EsriLegend({
            view: this.mapView,
            layerInfos: [
                {
                    layer: layers,
                    title: 'Saksi'
                }
            ]
        });
        this.legend.style = {
            type: 'classic',
            layout: 'auto'
        };
        var bgExpand = new this.EsriExpand({
            expandIconClass: 'esri-icon-description',
            expandTooltip: 'Info Legend',
            view: this.mapView,
            content: this.legend
        });
        this.maps.add(layers);
        this.mapView.ui.add(bgExpand, 'bottom-right');
        this.mapView.ui.padding = { top: 20, left: 20, right: 20, bottom: 40 };
    };
    EsriMapComponent.prototype.errback = function (error) {
        console.error('Creating legend failed. ', error);
    };
    EsriMapComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Initialize MapView and return an instance of MapView
        this.initializeMap().then(function (result) {
            _this.houseKeeping(result);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], EsriMapComponent.prototype, "mapLoadedEvent", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('mapViewNode'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], EsriMapComponent.prototype, "mapViewEl", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('infoDiv'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], EsriMapComponent.prototype, "infoDiv", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number),
        __metadata("design:paramtypes", [Number])
    ], EsriMapComponent.prototype, "zoom", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], EsriMapComponent.prototype, "center", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], EsriMapComponent.prototype, "basemap", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], EsriMapComponent.prototype, "ground", null);
    EsriMapComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-esri-map',
            template: __webpack_require__(/*! ./esri-map.component.html */ "./src/app/layout/peta/esri-map/esri-map.component.html"),
            styles: [__webpack_require__(/*! ./esri-map.component.css */ "./src/app/layout/peta/esri-map/esri-map.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], EsriMapComponent);
    return EsriMapComponent;
}());



/***/ }),

/***/ "./src/app/layout/peta/peta-routing.module.ts":
/*!****************************************************!*\
  !*** ./src/app/layout/peta/peta-routing.module.ts ***!
  \****************************************************/
/*! exports provided: PetaRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PetaRoutingModule", function() { return PetaRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _peta_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./peta.component */ "./src/app/layout/peta/peta.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _peta_component__WEBPACK_IMPORTED_MODULE_2__["PetaComponent"]
    }
];
var PetaRoutingModule = /** @class */ (function () {
    function PetaRoutingModule() {
    }
    PetaRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], PetaRoutingModule);
    return PetaRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/peta/peta.component.html":
/*!*************************************************!*\
  !*** ./src/app/layout/peta/peta.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n    <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n            <div style=\"height:600px\">\r\n                <app-esri-map\r\n                [center]=\"mapCenter\"\r\n                [basemap]=\"basemapType\"\r\n                [zoom]=\"mapZoomLevel\"\r\n                (mapLoadedEvent)=\"mapLoadedEvent($event)\">\r\n                </app-esri-map>\r\n            </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/layout/peta/peta.component.scss":
/*!*************************************************!*\
  !*** ./src/app/layout/peta/peta.component.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/layout/peta/peta.component.ts":
/*!***********************************************!*\
  !*** ./src/app/layout/peta/peta.component.ts ***!
  \***********************************************/
/*! exports provided: PetaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PetaComponent", function() { return PetaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../router.animations */ "./src/app/router.animations.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var PetaComponent = /** @class */ (function () {
    function PetaComponent() {
        // Set our map properties
        this.mapCenter = [107.574, -6.819];
        this.basemapType = 'osm';
        this.mapZoomLevel = 13;
    }
    // See app.component.html
    PetaComponent.prototype.mapLoadedEvent = function (status) {
        console.log('The map loaded: ' + status);
    };
    PetaComponent.prototype.ngOnInit = function () {
        this.innerHeight = window.innerHeight;
    };
    PetaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-peta',
            template: __webpack_require__(/*! ./peta.component.html */ "./src/app/layout/peta/peta.component.html"),
            styles: [__webpack_require__(/*! ./peta.component.scss */ "./src/app/layout/peta/peta.component.scss")],
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()]
        })
    ], PetaComponent);
    return PetaComponent;
}());



/***/ }),

/***/ "./src/app/layout/peta/peta.module.ts":
/*!********************************************!*\
  !*** ./src/app/layout/peta/peta.module.ts ***!
  \********************************************/
/*! exports provided: PetaModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PetaModule", function() { return PetaModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _peta_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./peta-routing.module */ "./src/app/layout/peta/peta-routing.module.ts");
/* harmony import */ var _peta_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./peta.component */ "./src/app/layout/peta/peta.component.ts");
/* harmony import */ var _esri_map_esri_map_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./esri-map/esri-map.component */ "./src/app/layout/peta/esri-map/esri-map.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var PetaModule = /** @class */ (function () {
    function PetaModule() {
    }
    PetaModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _peta_routing_module__WEBPACK_IMPORTED_MODULE_2__["PetaRoutingModule"]],
            declarations: [_peta_component__WEBPACK_IMPORTED_MODULE_3__["PetaComponent"], _esri_map_esri_map_component__WEBPACK_IMPORTED_MODULE_4__["EsriMapComponent"]]
        })
    ], PetaModule);
    return PetaModule;
}());



/***/ }),

/***/ "./src/app/router.animations.ts":
/*!**************************************!*\
  !*** ./src/app/router.animations.ts ***!
  \**************************************/
/*! exports provided: routerTransition, noTransition, slideToRight, slideToLeft, slideToBottom, slideToTop */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routerTransition", function() { return routerTransition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "noTransition", function() { return noTransition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideToRight", function() { return slideToRight; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideToLeft", function() { return slideToLeft; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideToBottom", function() { return slideToBottom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideToTop", function() { return slideToTop; });
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");

function routerTransition() {
    return noTransition();
}
function noTransition() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', []);
}
function slideToRight() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('*', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(-100%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }))
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(100%)' }))
        ])
    ]);
}
function slideToLeft() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('*', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(100%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }))
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(-100%)' }))
        ])
    ]);
}
function slideToBottom() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('*', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(-100%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(0%)' }))
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(0%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(100%)' }))
        ])
    ]);
}
function slideToTop() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('*', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(100%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(0%)' }))
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(0%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(-100%)' }))
        ])
    ]);
}


/***/ })

}]);
//# sourceMappingURL=peta-peta-module.js.map